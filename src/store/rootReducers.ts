import {scriptPlayerReducer} from 'ssp-script-player-react';

import {combineReducers} from 'redux';

export default combineReducers({
    scriptPlayerReducer: scriptPlayerReducer,
});
