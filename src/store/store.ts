import thunk from 'redux-thunk';

import {createStore, applyMiddleware, compose} from 'redux';
import rootReducers from "./rootReducers";


declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export default createStore(rootReducers, composeEnhancers(applyMiddleware(thunk)));

