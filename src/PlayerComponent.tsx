import React from 'react';
import {IScriptRs} from 'ssp-script-player-react';
import scriptText from "./scripts/script.json";
import {PagePlayer} from "./components/PlayerPage";

function PlayerComponent() {
    return (
        <PagePlayer script={(JSON.parse(JSON.stringify(scriptText)) as IScriptRs)} context={{}}/>
    )
}

export default PlayerComponent;