import React from 'react';
import './App.css';
import PlayerComponent from "./PlayerComponent";
import {Provider} from "react-redux";
import store from './store/store';

import 'antd/dist/antd.css'

function App() {
    return (
        <div>
            <Provider store={store}>
                <PlayerComponent/>
            </Provider>
        </div>
    );
}

export default App;
