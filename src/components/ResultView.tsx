import React from 'react';
import {Button, Result} from 'antd';
import {IPlayerContext} from "ssp-script-player-react";


interface IResultView {
    result: IPlayerContext;
    onRepeat: () => void;
}

export const ResultView = (props: IResultView) => (
    <Result
        data-test-id="ResultView.Component"
        status="success"
        title="Скрипт прошёл успешно!"
        extra={[
            <Button
                data-test-id="ResultView.action.repeat"
                type="primary"
                key="repeat"
                onClick={props.onRepeat}
            >
                Повторить
            </Button>,
        ]}
    >
        <pre data-test-id="ResultView.result">{JSON.stringify(props.result, null, 2)}</pre>
    </Result>
);
