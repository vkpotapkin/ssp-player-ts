import React, {useState} from 'react';
import {Divider, Form} from 'antd';

import {useDispatch} from 'react-redux';
import {IPlayerContext, IScriptRs, Player, restartAndRunScriptAction} from "ssp-script-player-react";
import {ResultView} from "./ResultView";

export const STORE_PATH = 'scriptPlayerReducer';
export const SCRIPT_NAME = 'firstScript';

function log() {
    console.log(arguments);
}

interface IPlayerView {
    script: IScriptRs;
    onResult: (res: IPlayerContext) => void;
    outsideContext?: IPlayerContext;
}

const PlayerView = (props: IPlayerView) => {
    return (
        <Form layout="vertical">
            <Divider>Плеер</Divider>

            <Player
                script={props.script}
                scriptName={SCRIPT_NAME}
                storePath={STORE_PATH}
                onResult={props.onResult}
                outsideContext={props.outsideContext ? props.outsideContext : {}}
                customWidgets={[]}
                onPhaseChange={log}
            />
        </Form>
    );
};

interface IProps {
    script: IScriptRs;
    context: IPlayerContext
}

export const PagePlayer = ({script, context}: IProps) => {
    const dispatch = useDispatch();

    //Добавление в текст своего свойства
    context = {myValue: "Мой текст"};

    const repeatHandler = () => {
        setResult(null);
        dispatch(
            restartAndRunScriptAction({
                script,
                startContext: context,
                storePath: STORE_PATH,
                scriptName: SCRIPT_NAME,
            }),
        );
    };

    const [result, setResult] = useState<IPlayerContext | null>(null);

    return (
        <div data-test-id="PagePlayer.Component" style={{padding: 20}}>
            {result === null ? (
                <PlayerView onResult={setResult} outsideContext={context} script={script}/>
            ) : (
                <ResultView result={result} onRepeat={repeatHandler}/>
            )}
        </div>
    );
};
